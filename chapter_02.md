# Creating and Destroying Objects

## Item 1: Static factory

* A class can provide its clients with static factory methods instead of, or in addition to, constructors. Advantages are:
    * Naming (`BigInteger(int, int, Random)` vs `BigInteger.probablePrime`)
    * Multiple methods with same signature
    * Not required to create new object on each call (Singleton?)
    * Can return objects of subclasses (reduced API footprint)
    * Reduces verbosity of generics

* Classes without public or protected constructors cannot be subclassed
    * Sometimes a good thing: encourages to use composition instead of inheritance

## Item 2: Builder construtor

* Telescoping constructor pattern: separate constructor for every combination of optional parameters
* JavaBeans constructor pattern: empty constructor + setters for each variable (kills immutability?)
* Builder constructor pattern: aux class that "builds" config for required variables and finally returns the class itself.
    * Emulates optional parameters functionality in Python.
    * Example: `NutritionFacts cocaCola = new NutritionFacts.Builder(240, 8).calories(100).sodium(35).build();`
    * Can support multiple var args.

## Item 3: Singleton

* A normal Singleton impl with `Serializable` must have all fields marked `transient` and also provide `readResolve()` method
* A single-element enum type is the best way to implement a singleton

## Item 4: Noninstantiability

* A class can be made noninstantiable by including a private constructor

---

* bounded wildcard type? `Builder<? extends Node>`?


